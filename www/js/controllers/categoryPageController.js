pptApp.controller('categoryPageController', function ($scope, $rootScope, $state, categoryService, $filter, $ionicModal, $ionicHistory, catListService, newProductDetailedService, $ionicScrollDelegate) {
    $ionicScrollDelegate.scrollTop();
    $scope.View = "grid";
    $scope.viewby = "12";
    $scope.sort_by = "popularty";
    $scope.fromVal = "0";
    $scope.pricerange = "0-10000";
    $scope.brand = "Powertex";
    $scope.toVal = "12";

    $scope.gotoFilter = function () {
        $scope.modal.show();
    }
    $ionicModal.fromTemplateUrl('templates/category_filter_page.html', {
        scope: $scope,
    }).then(function (modal) {
        $scope.modal = modal;
    });

    $scope.goback = function () {
        $ionicHistory.goBack();
    }

    $scope.getProductDetails = function (prodname) {
        window.localStorage['productName'] = prodname;
        $state.go("app.product_Detaile_page");
    }


    $scope.getnewCategories = function () {
        //$scope.loading = true;
        catListService.getNewCategoriesMethod().then(function (data) {
            //$scope.loading = false;              
            if (data.data.status == 'success') {
                $scope.categoryData = data.data.categories;

                $scope.toggleGroup = function (category) {
                    if ($scope.isGroupShown(category)) {
                        $scope.shownGroup = null;
                    } else {
                        $scope.shownGroup = category;
                    }
                };
                $scope.isGroupShown = function (category) {
                    return $scope.shownGroup === category;
                };
                //cat-list code end here 

            } else {
            }
        })
    }
    $scope.getnewCategories();

    $scope.subCategoryMethod = function (subCategory, categoryName) {
        window.localStorage['subcategory'] = subCategory;
        window.localStorage['category'] = categoryName;
        $state.go('app.categories_page');
    }
    $scope.category = window.localStorage['category'];

    $scope.categorylistfilter = function () {
        $scope.brand = [""];
        $scope.subcat = [window.localStorage['subcategory']];
        newProductDetailedService.getnewAllCategoriesFilterOfProduct(window.localStorage['category'], $scope.subcat, $scope.brand, $scope.pricerange, $scope.fromVal, $scope.toVal, $scope.sort_by).then(function (data) {
            if (data.data.status == 'success') {
                $scope.getCategorylist = data.data.value;
                $scope.subcatArray = data.data.filterdata.subcategory;
                $scope.nodata = false;
            }
            else if (data.data.status == 'No data avialbale') {
                $scope.nodata = true;
            }
        })
    }
    $scope.categorylistfilter();



});
