pptApp.controller('checkoutController', function ($scope, checkoutService, $ionicModal, $rootScope, $state, $ionicPopup, $ionicHistory, $cordovaInAppBrowser, $ionicScrollDelegate, $ionicLoading) {
    $ionicScrollDelegate.scrollTop();
    $scope.showpincodeDiv = true;
    $scope.showOrderDetailDiv = false;
    $scope.paymentDiv = false;
    // $scope.showShippingAddressDiv = false;
    $scope.placeOrderBtn = true;

    $ionicModal.fromTemplateUrl('templates/tools_shops_model.html', {
        scope: $scope,
    }).then(function (modal) {
        $scope.shopListmodal = modal;
    });

    $scope.saveData = function () {
        if ($scope.selectedShop != '' && $scope.selectedShop == undefined) {
            $ionicPopup.alert({
                title: 'Alert',
                template: 'Please Select Shop'
            });
        } else {
            $scope.shopListmodal.hide();
            $scope.showpincodeDiv = false;
            $scope.showOrderDetailDiv = true;
            $scope.showShippingAddressDiv = false;
        }
    }

    $scope.closeModal = function () {
        $scope.shopListmodal.hide();
    }

    // $scope.noshops = function () {
    //     $scope.shopListmodal.hide();
    //     $scope.showShippingAddressDiv = true;
    //     $scope.showpincodeDiv = false;
    // }

    $scope.getshoplist = function (pincode, altMobile) {
        $ionicLoading.show({
            template: '<ion-spinner icon="spiral"></ion-spinner>',
        });
        $scope.altMobile = altMobile;

        checkoutService.pincodecheckMethod(pincode).then(function (data) {
            //    console.log(JSON.stringify(data))
            if (data.data.status == 'Success') {
                $scope.delivery_addrShow = false;
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({ 'address': JSON.stringify(pincode) }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        var latitude = results[0].geometry.location.lat();
                        var longitude = results[0].geometry.location.lng();
                        // alert("Latitude: " + latitude + "\nLongitude: " + longitude);
                        $scope.latLongArray = [];
                        $scope.latLongArray.push(latitude, longitude);
                        $scope.getDealersList($scope.latLongArray, pincode)
                        $scope.getbtn = false;
                        $scope.enquiryinst = false;
                    } else {

                    }
                });
            }
            else {
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Alert',
                    template: 'your Enquiry sent to dealer. dealer will contact you soon'
                });

                $state.go("app.homePage");
                
                $scope.showOrderDetailDiv = true;
                $scope.showpincodeDiv = false;
            }
        })
    }

    $scope.getDealersList = function (latLongArray, pincode) {
        $ionicLoading.show();
        checkoutService.getDealersListMethod(latLongArray, pincode).then(function (data) {
            // alert(JSON.stringify(data));
            $ionicLoading.hide();
            if (data.data.status == 'success') {
                $scope.dealersList = data.data.dealer_list;
                $scope.shopListmodal.show();
            }
        })
    }

    $scope.saveShopData = function (shopsData) {
        $scope.selectedShop = shopsData;
        $scope.shop_name = shopsData.shop_name;
    }

    $scope.gotoPaymentType = function () {
        $scope.paymentDiv = true;
        $scope.showOrderDetailDiv = false;
        $scope.paymentType ="cashondelivery"
        $scope.placeOrder();
    }

    $scope.gotoPayment = function () {
       // $scope.paymentType = paymentType;
        $scope.paymentType ="cashondelivery"
        $scope.placeOrder();
        $scope.placeOrderBtn = false;
    }

    $scope.goback = function () {
        $ionicHistory.goBack();
    };

    $scope.editShippingAdd = function () {
        $state.go('app.editShippingModal');
    };
    $scope.gotoOrderDiv = function () {
        $scope.showOrderDetailDiv = true;
        $scope.showShippingAddressDiv = false;
    };


    $scope.orderItemArray = [];
    $rootScope.cartArray.forEach(function (cartItem) {
        $scope.orderItemArray.push({
            "sno": "", "productdescription": cartItem.upload_name, "qty": cartItem.qty,
            "unitprice": cartItem.mrp, "enduser_price": "", "total": JSON.stringify(((cartItem.mrp * cartItem.tax) / 100 * cartItem.qty) + (cartItem.mrp * cartItem.qty)),
            "tax": cartItem.tax, "tax_amount": JSON.stringify(cartItem.tax_amount), "sub_total": JSON.stringify(cartItem.mrp * cartItem.qty)
        })
    });
    console.log($scope.orderItemArray);

    $scope.customermobile = window.localStorage['mobile'];

    $scope.placeOrder = function () {


        $scope.orderArray = {
            "status": 'Accepted',
            "shop": $scope.shop_name,
            "alt_mobile": $scope.altMobile,
            "pic_alt_mobile": $scope.altMobile,
            "customermobile": $scope.customermobile,
            "totalamount": $rootScope.grand_total,
            "orderitems": $scope.orderItemArray,
            // "cupon_id": $scope.couponId,
            // "discount": $scope.couponAmt,
            "user_type": "web",
            "billingaddress": [],
            "shippingaddress": [],
            "shippingtype": "",
            "totalquantity": JSON.stringify($rootScope.cartArray.length),
            "paymenttype": $scope.paymentType,
            "total_items": JSON.stringify($rootScope.cartArray.length),
            "user_id": window.localStorage['user_id'],
            "gst_number": ''
        }
        console.log($scope.orderArray);

        checkoutService.saveOrderMethod($scope.orderArray).then(function (data) {
            if (data.data.status == "data saved") {
                $scope.finalOrderId = data.data.orderid;
               // alert($scope.finalOrderId);
                window.localStorage['finalOrderId'] = $scope.finalOrderId;
                $ionicPopup.alert({
                   title: 'Alert',
                    template: '<span>Dealer will contact you soon</span>&nbsp;&nbsp;{{finalOrderId}}'
                });
               // alert($scope.paymentType);
                if ($scope.paymentType == 'payu') {
                    $ionicPopup.alert({
                        title: 'Order ID',
                        template: '{{finalOrderId}}'
                    });
                    // alert('in');
                    $scope.payu();
                } else if($scope.paymentType == 'cashondelivery'){
                    // $ionicPopup.alert({
                    //     title: 'Order ID',
                    //     template: '{{finalOrderId}}'
                    // });
                    $state.go('app.homePage');
                }
                
            }
        })



    }

    $scope.sendEnquiry = function () {
        checkoutService.sendEnquiry($scope.shop_name).then(function (data) {
            alert(JSON.stringify(data));
            if (data.data.status == "Success") {
                alert("your Enquiry sent to dealer. dealer will contact you soon");
                $state.go("app.homePage");
            }
        })

    }



    /// PAYU

    $scope.getPayuDetails = function () {
        checkoutService.getpayuDetailsMethod().then(function (data) {
            if (data.data.status == 'payu data') {
                $scope.payuData = data.data.data;
                window.localStorage['merchant_key'] = $scope.payuData.merchant_id;
                window.localStorage['salt_key'] = $scope.payuData.salt_key;
            }
        })
    }
    $scope.getPayuDetails();

    $scope.firstname = window.localStorage['user_id'];
    $scope.phone = window.localStorage['mobile'];

    $scope.payu = function () {
        checkoutService.saveOrderMethod($scope.orderArray).then(function (data) {
             $ionicLoading.show();
            if (data.data.status == 'data saved') {
                $ionicLoading.hide();
                $scope.finalOrderId = data.data.orderid;
                $rootScope.finalOrderId = $scope.finalOrderId;
                window.localStorage['finalOrderId'] = $scope.finalOrderId;
                var options = {
                    location: 'yes',
                    clearcache: 'yes',
                    toolbar: 'no',
                    closebuttoncaption: 'back'
                };
                if ($scope.paymentType == 'payu') {
                    // onDeviceReadyTest()
                    $ionicLoading.hide();

                    var amt = parseInt($rootScope.grand_total);
                    var name = $scope.firstname;
                    var mobile = $scope.phone;
                    var email = window.localStorage['email'];
                    var bookingId = window.localStorage['finalOrderId'];
                    var productinfo = "Order for OR0293435435";
                    var key = window.localStorage['merchant_key'];
                    var salt = window.localStorage['salt_key'];
                    var string = key + '|' + bookingId + '|' + amt + '|' + productinfo + '|' + name + '|' + email + '|||||||||||' + salt;
                    var encrypttext = sha512(string);

                    var data = 'key=' + key
                        + '&txnid=' + bookingId
                        + '&amount=' + amt
                        + '&productinfo=' + productinfo
                        + '&firstname=' + name
                        + '&email=' + email
                        + '&phone=' + mobile
                        + '&hash=' + encrypttext;


                    var browser = $cordovaInAppBrowser.open('templates/payu.html?' + 'key=' + key
                        + '&txnid=' + bookingId
                        + '&amount=' + amt
                        + '&productinfo=' + productinfo
                        + '&firstname=' + name
                        + '&email=' + email
                        + '&phone=' + mobile
                        + '&hash=' + encrypttext, '_blank', options, 'closebuttoncaption=Done').then(function (event) {
                            // success
                            //  alert(JSON.stringify(event))
                        })
                        .catch(function (event) {
                            // error
                        });

                    $rootScope.$on('$cordovaInAppBrowser:loadstart', function (e, event) {
                        // alert('loadstart'+JSON.stringify(event))

                    });

                    $rootScope.$on('$cordovaInAppBrowser:loadstop', function (e, event) {
                        // alert('loadstop'+JSON.stringify(event))
                        if (event.url == 'http://toolsomg.com/success.html') {

                            $cordovaInAppBrowser.close();
                            // alert(window.localStorage['finalOrderId'])
                            //     $scope.submitPayment(window.localStorage['finalOrderId']);
                            $state.go('app.homePage')

                        }
                    });

                }


            }

        })
    }

});