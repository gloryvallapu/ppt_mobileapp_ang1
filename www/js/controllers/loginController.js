pptApp.controller('loginController', function ($scope, $rootScope, $state, loginService, registrationService, $ionicPopup, logoutService, $ionicLoading, $ionicHistory, $window, $ionicScrollDelegate) {

    $ionicScrollDelegate.scrollTop();
    $scope.loginppt = function (loginData) {
        $scope.username = loginData.username;
        $scope.password = loginData.password;;

        loginService.loginpptAuthentication($scope.username, $scope.password).then(function (data) {
            location.reload();
            if (data.data.status == "more than one usertypes") {
                $scope.usertypes = data.data.user_types;
                $scope.single_usertpe = true;
            } else if (data.data.status = "success") {
                if (data.data.usertype == "web") {
                    $scope.single_usertpe = false;
                    $scope.usertype = data.data.usertype;
                    $rootScope.token = data.data.token;
                    window.localStorage['usertype'] = $scope.usertype;
                    window.localStorage['token'] = data.data.token;
                    window.localStorage['user_id'] = data.data.user_id;
                    window.localStorage['email'] = data.data.userinfo.email;
                    window.localStorage['mobile'] = data.data.userinfo.user_mobile;
                    window.localStorage['user_name'] = data.data.username;
                    window.localStorage['gst_number'] = data.data.GSTnumber;
                    window.localStorage['firstname'] = data.data.userinfo.firstname;
                    localStorage.setItem('userInfo', JSON.stringify(data.data.userinfo));
                    localStorage.setItem('shippingAddressInfo', JSON.stringify(data.data.shipping_address));
                    localStorage.setItem('billingAddressInfo', JSON.stringify(data.data.billing_address));
                    $state.go('app.homePage');
                } else {
                    alert('Please Login Customer Credentials');
                }
            }
        })
    }

    //Customer
    $scope.customerLogin = function (loginData) {
        if ($scope.loginForm.$valid) {
            loginService.userAuthentication(loginData.username, loginData.password, $rootScope.ip).then(function (data) {

                if (data.data.status == 'Success') {
                    $scope.wishlistShow = true;
                    window.localStorage['username'] = data.data.username;
                    window.localStorage['token'] = data.data.token;
                    window.localStorage['user_id'] = data.data.user_id;
                    $scope.userName = data.data.username;
                    window.localStorage['email'] = data.data.userinfo.email;
                    window.localStorage['mobile'] = data.data.userinfo.user_mobile;
                    window.localStorage['customermobile'] = data.data.userinfo.user_mobile;
                    localStorage.setItem('userInfo', JSON.stringify(data.data.userinfo));
                    window.localStorage['usertype'] = data.data.userinfo.user_type;
                    if (data.data.GSTnumber != '') {
                        localStorage.setItem('gstNumber', data.data.GSTnumber);
                    } else {
                        localStorage.setItem('gstNumber', '');
                    }
                    localStorage.setItem('shippingAddressInfo', JSON.stringify(data.data.shipping_address));
                    localStorage.setItem('billingAddressInfo', JSON.stringify(data.data.billing_address));
                    window.localStorage['user_name'] = $scope.userName;
                    if ($scope.previousUrl == 'viewCart') {
                        $scope.currentUrl = $scope.previousUrlArray[0].concat("#!/checkout");
                        window.location.href = $scope.currentUrl;
                    } else {
                        window.location.href = "./homePage.html";
                    }
                } else {
                }
            })
        }

    }

    $scope.gotoRegistration = function () {
        $state.go('app.registration');
    };

    $scope.register = function (registrationData) {
        $scope.registerForm = {};
        if (!registrationData.gstnumber) {
            registrationData.gstnumber = "";
        }
        window.localStorage['userInfo'] = registrationData;
        localStorage.setItem('userInfo', JSON.stringify(registrationData));
        //   
        // if ($scope.registerForm.$valid) {
        //   if (registrationData.newsletter == true) {
        //     registrationData.newsletter = "checked";
        //   } else {
        //     registrationData.newsletter = "unchecked";
        //   }
        $rootScope.registrationData = registrationData;
        window.localStorage['mobile'] = $rootScope.registrationData.mobile;
        window.localStorage['customermobile'] = $rootScope.registrationData.mobile;
        registrationService.userRegistration($rootScope.registrationData, $rootScope.usertype).then(function (data) {
            alert(JSON.stringify(data))
            if (data.data.status == 'Data saved successfully') {
                $scope.showPopup();
            } else {
                alert(data.data.status)
            }
        })
        // }

    }

    $scope.showPopup = function () {
        // An elaborate, custom popup
        $scope.myPopup = $ionicPopup.show({
            templateUrl: '/templates/otp_popup.html',
            title: 'Enter OTP',
            scope: $scope,
        });
    }

    $scope.completeRegistrtion = function (otp) {
        if (otp == undefined) {
            $scope.error = true;
        } else {
            $scope.error = false;
            if (typeof ($scope.otp) == 'string') {
                $scope.otp = otp
            } else {
                $scope.otp = JSON.stringify(otp)
            }

            registrationService.verifyOTP($scope.otp, window.localStorage['mobile'], $rootScope.ip).then(function (data) {
                //alert(JSON.stringify(data))

                if (data.data.status == 'Data saved successfully') {
                    window.localStorage['token'] = data.data.token;
                    window.localStorage['user_id'] = data.data.user_id;
                    window.localStorage['email'] = $rootScope.registrationData.email;
                    window.localStorage['user_name'] = $rootScope.registrationData.firstname;
                    // window.location.href = "./index.html";
                } else if (data.data.status == 'Mobile no / email already exists..') {
                    alert(data.data.status + 'Please Login');
                }
                else {
                    alert('Enter valid OTP')
                }
            })
        }

    }

    $scope.resendOTP = function () {
        registrationService.resendOTP(window.localStorage['mobile']).then(function (data) {
            // alert(JSON.stringify(data))
            if (data.data.return == "otp sent to mobile") {
                $scope.myPopup.close();
                $ionicPopup.alert({
                    title: 'Alert',
                    template: 'otp sent to mobile'
                });
                confirmPopup.then(function (res) {
                    if (res) {
                        console.log('ok');
                        $scope.showPopup();
                        $state.go("app.homePage");
        
                    } else {
                        console.log('cancel');
                        $state.go("app.homePage");
                    }
                });

                
            }



        })
    }

    $scope.logout = function () {

        var confirmPopup = $ionicPopup.confirm({
            title: 'Logout',
            template: 'Are you sure want to logout'
        });

        confirmPopup.then(function (res) {
            if (res) {
                console.log('ok');
                $scope.confirmlogout();

            } else {
                console.log('cancel');
                $state.go("app.homePage");
            }
        });

    }



    $scope.confirmlogout = function () {


        logoutService.userLogout(window.localStorage['token']).then(function (data) {

            if (data.data.status == 'success') {
                $window.localStorage.clear();
                $ionicHistory.clearCache();
                $ionicHistory.clearHistory();
                $state.go("app.homePage");
                location.reload();

            }
        })
    }




    $scope.gohome = function () {
        $state.go("app.homePage");
    }

});

pptApp.controller('userprofileController', function ($scope, $state, $rootScope, $ionicPopup, $timeout, userProfileService, $ionicScrollDelegate) {
    $ionicScrollDelegate.scrollTop();
    $scope.userprofile = {};
    $scope.userprofile = function () {
        $scope.usertype = "web";
        userProfileService.userprofileDetails(window.localStorage['user_id'], $scope.usertype).then(function (data) {
            if (data.data.status == "Success") {
                $scope.userprofile = data.data.profile_info;
            }
        })
    }
    $scope.userprofile();

    $scope.editmobilenumsubmit = function (data) {
        $scope.mobile_no = data;
        userProfileService.editmobilenum(window.localStorage['user_id'], $scope.mobile_no).then(function (data) {
            if (data.data.status == "Mobile Number Saved successfully.") {
                window.localStorage['mobile_no'] = data.data.mobile;
                $scope.showPopup();
            }
        })

    }
    $scope.enterotp = function (otp) {
        $scope.otp = otp;
        userProfileService.enterotp(window.localStorage['user_id'], window.localStorage['mobile_no'], $scope.otp, window.localStorage['usertype']).then(function (data) {
            location.reload();
            if (data.data.status == "Mobile Number Saved successfully.") {
                window.localStorage['mobile_no'] = data.data.mobile;
                window.localStorage['mobile'] = data.data.mobile;

            }
        })

    }

    $scope.showPopup = function () {
        $scope.data = {};

        // An elaborate, custom popup
        var myPopup = $ionicPopup.show({
            template: '<input type="password" ng-model="data.wifi">',
            title: 'Enter OTP',
            scope: $scope,
            buttons: [
                { text: 'Cancel' },
                {
                    text: '<b>Save</b>',
                    type: 'button-positive',
                    onTap: function (e) {
                        if (!$scope.data.wifi) {
                            //don't allow the user to close unless he enters wifi password
                            e.preventDefault();
                        } else {
                            return $scope.data.wifi;
                        }
                    }
                }
            ]
        });
        myPopup.then(function (res) {
            console.log('Tapped!', res);
            $scope.enterotp(res);
        });

        //   $timeout(function() {
        //      myPopup.close(); //close the popup after 3 seconds for some reason
        //   }, 3000);
    };

});



pptApp.controller('forgotPasswordCntrl', function ($scope, $state, $rootScope, forgotPasswordService, $ionicPopup, $ionicScrollDelegate) {
    $ionicScrollDelegate.scrollTop();
    $scope.gotoResetPswd = function (email) {
        forgotPasswordService.forgotPassword(email).then(function (data) {
            if (data.data.status == "email sent") {
                $scope.time_stamp = data.data.time_stamp;
                $ionicPopup.alert({
                    title: 'Alert',
                    template: data.data.status
                });
                //  alert(data.data.status);
                $state.go('app.homePage');
            } else {
                $ionicPopup.alert({
                    title: 'Alert',
                    template: data.data.status
                });
                //  alert(data.data.status);
            }
        })
    }

});