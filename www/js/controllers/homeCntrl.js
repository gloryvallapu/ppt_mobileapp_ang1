pptApp.controller('homeController', function ($scope, $rootScope, $state, newHomePageService, $filter, $ionicScrollDelegate) {
    $ionicScrollDelegate.scrollTop();
    $scope.getNewHeaderData = function (obj) {

        $scope.today = $filter('date')(new Date(), 'dd-MM-yyyy');

        newHomePageService.newHomePageMethod(obj, $scope.today).then(function (data) {
            if (data.data.status == 'Success') {
                $scope.headerLogoData = data.data.HeaderLogo;
                $scope.headerLogoData.forEach(function (element) {
                    $scope.headerLogoData = element;
                }, this);
                $rootScope.bannerArray = data.data.BannerData;
                $rootScope.shopmytools = data.data.Footer;
                $rootScope.customerservice = data.data.Footer2;
                $scope.phonenumber = data.data.SitePhnNbr;
                $scope.phonenumber.forEach(function (element) {
                    $scope.phonenumber = element;
                }, this);
            }
        })
    }
    $scope.getNewHeaderData("Powertex");

    //offers service integration
    $scope.getoffersData = function (categoryobj) {
        $scope.userid = window.localStorage['user_id'];
        newHomePageService.offersMethod(categoryobj, $scope.userid).then(function (data) {
            if (data.data.status == 'Success') {
                $rootScope.offersdata = data.data.offer_products;
            }
        })
    }
    $scope.getoffersData("")

    //new arraivals service integration
    $scope.getnewArrivalData = function (categoryObj) {
        $scope.userid = window.localStorage['user_id'];
        newHomePageService.newArrivalMethod(categoryObj, $scope.userid).then(function (data) {
            if (data.data.status == 'Success') {
                $rootScope.newarrivalsData = data.data.Newarrivals_products;
            }
        })
    }
    $scope.getnewArrivalData("")

    //swiper for offers and arraivals 
    $scope.galleryOptions = {
        pagination: '.swiper-pagination',
        slidesPerView: 5,
        freeMode: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        paginationClickable: true,
        spaceBetween: 5
    };
    $scope.galleryOptions_deal = {
        pagination: '.swiper-pagination',
        slidesPerView: 2.2,
        freeMode: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        paginationClickable: true,
        spaceBetween: 5
    };
    $scope.options = {
        autoplay: 2000,
        loop: true
    }

    //Product Detaile page click
    $scope.getProductDetails = function (prodname) {
        $rootScope.productName = prodname;
        window.localStorage['productName'] = prodname;
        $state.go("app.product_Detaile_page");
        
    }


    $scope.offers_products_page = function () {
        $state.go("app.all_offers");
    }

    $scope.newarraivals_products_page = function () {
        $state.go("app.all_newarraivals");
    }


    $scope.categoryBasedProducts = function (categoryName) {
        window.localStorage['category'] = categoryName;
        window.localStorage['subcategory'] = [];
        $state.go("app.categories_page");

    }

    $scope.search = function () {
        $state.go("app.header_searchBar");
    };

    $scope.allItemcount = function () {
        newHomePageService.getAllItemscount(window.localStorage['user_id'], window.localStorage['usertype']).then(function (data) {
            if (data.data.status == "success") {
                $rootScope.totalItems = data.data.cart_count;
                $rootScope.wishlistItems = data.data.wishlist_count;
            } else {

            }
        })
    }

    if (window.localStorage.length > 0) {
        $scope.allItemcount();
    }


    $scope.homePage = function () {
        $state.go("app.homePage");
    }

    $scope.categories_page = function () {
        $state.go("app.cat_list");
    };



});

pptApp.controller('searchController', function ($scope, $rootScope, $state, searchService, $ionicScrollDelegate) {
    $ionicScrollDelegate.scrollTop();
    $rootScope.showHintFlag = 'false';
   

    $rootScope.showHint = function (searchKey) {
        if (searchKey.length >= '3') {

            searchService.searchProductsMethod(searchKey).then(function (data) {

                if (data.data.status == 'success') {

                    $scope.searchedProducts = data.data.product_info;


                    $scope.recommendedList = data.data.recommended;

                    $rootScope.showHintFlag = 'true';



                } else if (data.data.status == 'fail') {

                    //  alert(data.data.data)

                    $scope.searchedProducts = [];
                    $rootScope.showHintFlag = 'true';
                   

                }

            })

        }
        else if (searchKey.length == '0') {

            $rootScope.showHintFlag = 'false';
            // $location.path("/")

        }

    }

    $scope.goback = function () {
        $state.go("app.homePage");
    };

    // $scope.gotoProductDetailPage = function (productName) {
    //     $scope.productName = productName;
    //     window.localStorage['productName'] = productName;
    //     $state.go("app.product_Detaile_page");
      
    // };

    $scope.getProductDetails = function (prodname) {
        location.reload();
        $rootScope.productName = prodname;
        window.localStorage['productName'] = prodname;
        $state.go("app.product_Detaile_page");
       
    }

    $scope.categoryBasedProducts = function (categoryName) {
        alert(categoryName);
        location.reload();
        window.localStorage['category'] = categoryName;
        window.localStorage['subcategory'] = [];
        $state.go("app.categories_page");
       
    }

});





