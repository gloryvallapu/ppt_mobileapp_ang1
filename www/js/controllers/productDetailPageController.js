pptApp.controller('productDetailController', function ($scope, $rootScope, $state, product_detailed_service, CartService, wishListService, $stateParams, $ionicPopup, $ionicHistory, $ionicScrollDelegate) {

    $ionicScrollDelegate.scrollTop()
    $scope.productName = $rootScope.productName;
    $scope.productName = window.localStorage['productName'];
    $scope.getnewProductDetailData = function () {
        product_detailed_service.getnewDetailsOfProduct($scope.productName).then(function (data) {
           
            if (data.data.status == 'success') {
                var result = data.data;
                $scope.brandDetailProductData = result.Product;
                $scope.productImagesLow = result.Low_Images;
                $scope.productImagesHigh = result.High_Images;
                $scope.productFirstImg = $scope.productImagesHigh[0];
                $scope.productdetailPrices = result.price_info;
                $rootScope.productattributes = result.attribute_info;
                $scope.relatedProducts = data.data.Related_Products;
                $scope.upsellProducts = result.Upsell_products;
            }
        })
    }
    $scope.getnewProductDetailData();

    // $scope.getProductDetails();

    $scope.getProductDetails = function (productObj) {  
        window.localStorage['productName'] = productObj.upload_name;
        $scope.productName = productObj.upload_name;
        $state.go("app.product_Detaile_page")
       
       

    }
    $scope.getProductDetails_two = function (productObj) {
        window.localStorage['productName'] = productObj.upload_name;
        $scope.productName = productObj.upload_name;
        $state.go("app.product_detail2")
        

    }


    $scope.viewCartPage = function () {
        //$scope.selectedButton = s;
        $state.go('app.cart_Page');
    }

    $scope.specifications = function () {
        //$scope.selectedButton = s;
        $state.go('app.specifications_page');
    }

    $scope.cartList = [];
    $rootScope.cartpopup = false;
    $scope.cartimg = false;
    $scope.qty = "1";

    $scope.addTocart = function (productobj, qty) {
        if ($rootScope.token != '' && $rootScope.token != undefined) {
            $scope.random_no = "";
            $scope.userId = window.localStorage['user_id'];
        } else {
            // alert('kjhkh');
            if (window.localStorage['random_no'] == undefined) {
                $scope.random_no = "";
            } else {
                $scope.random_no = window.localStorage['random_no'];
            }
            $scope.userId = "";
        }
        // $scope.random_no = "";
        // $scope.userId = window.localStorage['user_id'];
        $rootScope.product_name = productobj.upload_name;

        if (qty != '' && qty != undefined) {
            $scope.quantity = qty;
        } else {
            $scope.quantity = productobj.qty;
        }

        $scope.cartItemsList = [];
        $scope.cartItemsList.push({ "productdescription": $rootScope.product_name, "qty": $scope.quantity });
        if ($scope.cartArray.length > 0) {
            for (i = 0; i < $scope.cartArray.length; i++) {
                if ($rootScope.product_name != $scope.cartArray[i].upload_name) {
                    $scope.cartItemsList.push({ "productdescription": $scope.cartArray[i].upload_name, "qty": $scope.cartArray[i].qty });
                }
                else if ($rootScope.product_name == $scope.cartArray[i].upload_name) {
                    $rootScope.cartpopup = true;
                    $ionicPopup.alert({
                        title: 'Alert',
                        template: 'Product Already Added'
                    });
                }

            }
        }

        CartService.addToCartcustomer($scope.cartItemsList, $scope.random_no, $scope.userId).then(function (data) {
            if (data.data.status == 'item added to cart') {
                window.localStorage['random_no'] = data.data.random_number;
                $ionicPopup.alert({
                    title: 'Alert',
                    template: data.data.status
                });
                $scope.viewcart();
            } else {
                $ionicPopup.alert({
                    title: 'Alert',
                    template: data.data.status
                });
            }
        })
    }

    $scope.viewcart = function () {
        if ($rootScope.token != '' && $rootScope.token != undefined) {
            $scope.random_no = "";
            $scope.userId = window.localStorage['user_id'];
        } else {


            if (window.localStorage['random_no'] == undefined) {
                $scope.random_no = "";
            } else {
                $scope.random_no = window.localStorage['random_no'];
            }

            $scope.userId = '';
            // alert($scope.userId);
        }

        // $scope.random_no = "";
        // $scope.userId = window.localStorage['user_id'];

        CartService.viewCartcustomer($scope.random_no, $scope.userId).then(function (data) {
            if (data.data.status == 'Success') {
                $rootScope.cartArray = data.data.total_item_list;
                $rootScope.total_price = data.data.total_price;
                $rootScope.total_tax_amt = data.data.total_tax_amt;
                $rootScope.cartArrayItems = $scope.cartArray;
                $rootScope.grand_total = data.data.grand_total;
                $rootScope.totalItems = data.data.total_item_list.length;
            } else if (data.data.status == 'cart is empyt.') {
                $scope.cartArray = [];
            }
        })
    }
    $scope.viewcart();


    $scope.gotoBuy = function () {
        $scope.addobj = {
            "upload_name": window.localStorage['productName'],
        }
        $scope.addTocart($scope.addobj, 1);
    }


    // get Quantity 
    // $scope.getQuantity = function (qty, prodobj) {
    //     $scope.qty = qty;
    //     $scope.productname = prodobj.upload_name;
    //     if ($rootScope.token != '' && $rootScope.token != undefined) {
    //         $scope.random_no = window.localStorage['random_no'];
    //         $scope.userId = window.localStorage['user_id'];
    //     } else {
    //         $scope.random_no = window.localStorage['random_no'];
    //         $scope.userId = "";
    //     }
    //     CartService.updateviewCartcustomer($scope.productname, $scope.qty, $scope.random_no, $scope.userId).then(function (data) {
    //         if (data.data.status == 'qty updated successfully') {
    //             $scope.viewcart();
    //         }
    //     })
    // }

    //delete cart item in cart
    // $scope.deletecartItem = function (productobj) {
    //     if (productobj == "all") {
    //         $scope.productname = productobj;
    //     } else {
    //         $scope.productname = productobj.upload_name;
    //     }
    //     // if ($rootScope.token != '' && $rootScope.token != undefined) {
    //     //     $scope.userId = window.localStorage['user_id'];
    //     //     $scope.random_no = "";
    //     // } else {
    //     //     $scope.userId = "";
    //     //     $scope.random_no = window.localStorage['random_no'];
    //     // }
    //     $scope.random_no = "";
    //     $scope.userId = window.localStorage['user_id'];
    //     CartService.deleteCartItemcust($scope.productname, $scope.userId, $scope.random_no).then(function (data) {
    //         if (data.data.status == 'product deleted successfully') {
    //             $scope.viewcart();

    //             $ionicPopup.alert({
    //                 title: 'Alert',
    //                 template: data.data.status
    //             });

    //             //alert(data.data.status);
    //         }
    //     })
    // }

    // checkout 
    // $scope.checkout_page = function (cartObj, grandtotal) {
    //     $rootScope.grandTotal = grandtotal;
    //     $rootScope.cartorderObj = cartObj;
    //     if (window.localStorage['token']) {
    //         $state.go("app.shipping&billing_page");
    //     } else {
    //         $ionicPopup.alert({
    //             title: 'Alert',
    //             template: 'Please Login for Checkout'
    //         });

    //         $state.go("app.login");
    //     }
    // }

    //quantity increse and decrese 
    // $scope.decreaseValue = function (obj) {
    //     if (obj.qty > 1) {
    //         $scope.qty--;
    //         $scope.getQuantity($scope.qty, obj)
    //     }
    // }

    // $scope.increaseValue = function (obj) {
    //     $scope.qty++;
    //     $scope.getQuantity($scope.qty, obj)
    // }

    $scope.goback = function () {
        // $state.go("app.homePage");
        $ionicHistory.goback();
    }


    $scope.addToWishList = function (productObj) {

        // alert(window.localStorage['token']);
        if (window.localStorage['token']) {
            wishListService.addNewWishListMethod(window.localStorage['user_id'], productObj).then(function (data) {
                if (data.data.status == 'product saved successfully') {
                    $ionicPopup.alert({
                        title: 'Alert',
                        template: data.data.status
                    });
                } else {
                    $ionicPopup.alert({
                        title: 'Alert',
                        template: data.data.status
                    });
                }

            })
        } else {

            alert('Please Login to Add To WishList')
        }


    }


});


pptApp.controller('wishListController', function ($scope, $rootScope, wishListService, $state, CartService, $ionicPopup, $ionicScrollDelegate) {
    $ionicScrollDelegate.scrollTop()
    $scope.getWishlistItems = function () {

        wishListService.yourWishlistMethod(window.localStorage['user_id']).then(function (data) {

            if (data.data.status == 'success') {

                $scope.yourWishlist = data.data.prod_info;

                $scope.data = $rootScope.yourWishlist;
                $scope.viewby = 5;
                $scope.wishlistItems = data.data.prod_info.length;
                $rootScope.wishlistItems = data.data.prod_info.length;

                $scope.currentPage = 1;
                $scope.itemsPerPage = $scope.viewby;
            } else {
                $ionicPopup.alert({
                    title: 'Alert',
                    template: data.data.status
                });
            }

        })
    }
    $scope.getWishlistItems();

    $scope.removeWishListItem = function (wishListObj) {

        var confirmPopup = $ionicPopup.confirm({
            title: 'Delete',
            template: 'Are you sure you want to remove this item from WishList?'
        });

        confirmPopup.then(function (res) {
            if (res) {
                console.log('You are sure');
                $scope.removeItemFromWishList(wishListObj.upload_name);
            } else {
                console.log('You are not sure');
            }
        });
    };


    $scope.removeItemFromWishList = function (upload_name) {
        wishListService.removeWishListItemMethod(window.localStorage['user_id'], upload_name).then(function (data) {
            //    alert(JSON.stringify(data))
            if (data.data.status == 'product removed successfully') {
                $ionicPopup.alert({
                    title: 'Alert',
                    template: data.data.status
                });
            } else {
                $ionicPopup.alert({
                    title: 'Alert',
                    template: data.data.status
                });
            }
            $scope.getWishlistItems();
        })
    }


    $scope.cartItemsList = [];
    $scope.random_no = "";
    $scope.addTocart = function (productName) {
        $scope.cartItemsList.push({ "productdescription": productName, "qty": 1 });
        CartService.addToCartcustomer($scope.cartItemsList, $scope.random_no, window.localStorage['user_id']).then(function (data) {
            if (data.data.status == 'item added to cart') {
                $ionicPopup.alert({
                    title: 'Alert',
                    template: data.data.status
                });
            } else {
                $ionicPopup.alert({
                    title: 'Alert',
                    template: data.data.status
                });
            }
        })
    }


    $scope.goback = function () {
        $state.go("app.homePage");
    }

    $scope.getProductDetails = function (prodname) {
        $rootScope.productName = prodname;
        window.localStorage['productName'] = prodname;
        $state.go("app.product_Detaile_page");
    }

});


pptApp.controller('viewCartController', function ($scope, $rootScope, CartService, $state, $ionicPopup, wishListService, $ionicScrollDelegate) {
    $ionicScrollDelegate.scrollTop()
    $scope.viewcart = function () {
        if ($rootScope.token != '' && $rootScope.token != undefined) {
            $scope.random_no = "";
            $scope.userId = window.localStorage['user_id'];
        } else {
            if (window.localStorage['random_no'] == undefined) {
                $scope.random_no = "";
            } else {
                $scope.random_no = window.localStorage['random_no'];
            }
            $scope.userId = '';
        }
        // $scope.random_no = "";
        // $scope.userId = window.localStorage['user_id'];

        CartService.viewCartcustomer($scope.random_no, $scope.userId).then(function (data) {
            if (data.data.status == 'Success') {
                $rootScope.cartArray = data.data.total_item_list;
                $rootScope.total_price = data.data.total_price;
                $rootScope.total_tax_amt = data.data.total_tax_amt;
                $rootScope.cartArrayItems = $scope.cartArray;
                $rootScope.grand_total = data.data.grand_total;
                $rootScope.totalItems = data.data.total_item_list.length;
            } else if (data.data.status == 'cart is empyt.') {
                $scope.cartArray = [];
                $rootScope.totalItems = "0";
            }
        })
    }
    $scope.viewcart();

    $scope.goback = function () {
        $state.go('app.homePage');
    };

    $scope.deletecartItem = function (productobj) {
        if (productobj == "all") {
            $scope.productname = productobj;
        } else {
            $scope.productname = productobj.upload_name;
        }
        if ($rootScope.token != '' && $rootScope.token != undefined) {
            $scope.userId = window.localStorage['user_id'];
            $scope.random_no = "";
        } else {
            $scope.userId = "";
            $scope.random_no = window.localStorage['random_no'];
        }
        // $scope.random_no = "";
        // $scope.userId = window.localStorage['user_id'];



        var confirmPopup = $ionicPopup.confirm({
            title: 'Delete',
            template: 'Are you sure you want to remove this item from Cart?'
        });

        confirmPopup.then(function (res) {
            if (res) {
                console.log('You are sure');
                $scope.DeleteCartItem($scope.productname, $scope.userId, $scope.random_no);
            } else {
                console.log('You are not sure');
            }
        });

    };

    $scope.DeleteCartItem = function (productname, userId, random_no) {
        CartService.deleteCartItemcust(productname, userId, random_no).then(function (data) {
            if (data.data.status == 'product deleted successfully') {
                $ionicPopup.alert({
                    title: 'Alert',
                    template: data.data.status
                });
                $scope.viewcart();
            } else {
                alert(data.data.status);
            }
        })
    }

    $scope.checkout_page = function (cartObj, grandtotal) {
        $rootScope.grandTotal = grandtotal;
        $rootScope.cartorderObj = cartObj;
        if (window.localStorage['token']) {
            $state.go("app.shipping&billing_page");
        } else {
            $ionicPopup.alert({
                title: 'Alert',
                template: 'Please Login for Checkout'
            });

            $state.go("app.login");
        }
    };

    $scope.decreaseValue = function (obj) {
        if (obj.qty > 1) {
            $scope.qty = JSON.parse(obj.qty) - 1;
            $scope.getQuantity($scope.qty, obj)
        }
    }

    $scope.increaseValue = function (obj) {
        $scope.qty = JSON.parse(obj.qty) + 1;
        $scope.getQuantity($scope.qty, obj)
    };

    $scope.getQuantity = function (qty, prodobj) {
        $scope.qty = qty;
        $scope.productname = prodobj.upload_name;
        if ($rootScope.token != '' && $rootScope.token != undefined) {
            $scope.random_no = "";
            $scope.userId = window.localStorage['user_id'];
        } else {
            $scope.random_no = window.localStorage['random_no'];
            $scope.userId = "";
        }
        // $scope.random_no = "";
        // $scope.userId = window.localStorage['user_id'];
        CartService.updateviewCartcustomer($scope.productname, $scope.qty, $scope.random_no, $scope.userId).then(function (data) {
            if (data.data.status == 'qty updated successfully') {
                $scope.viewcart();
            }
        })
    };

    $scope.addToWishList = function (productObj) {
        // alert(window.localStorage['token']);
        if (window.localStorage['token']) {
            wishListService.addNewWishListMethod(window.localStorage['user_id'], productObj).then(function (data) {
                if (data.data.status == 'product saved successfully') {
                    $ionicPopup.alert({
                        title: 'Alert',
                        template: data.data.status
                    });
                } else {
                    $ionicPopup.alert({
                        title: 'Alert',
                        template: data.data.status
                    });
                }

            })
        } else {

            alert('Please Login to Add To WishList')
        }

    }



});

