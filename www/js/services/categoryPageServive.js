pptApp.service('categoryService', function ($q, $http, HOMEPAGE_SERVICE) {

    /* categories Service start here */
    this.getdistributortcatget = function (category, subcategory, brand) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: HOMEPAGE_SERVICE + '/catsubcatbrandget?category=' + category + '&sub_category=' + subcategory + '&brand=' + brand,
            headers: { 'Content-Type': 'application/json', "secret_key": "4r5t@W", 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8' },
            data: { "category": category, "sub_category": subcategory, "brand": brand }
        }).then(function success(data) {
            deferred.resolve(data);
        }, function error(data) {
            deferred.reject(data);
        });
        return deferred.promise;
    };

});

pptApp.service('newProductDetailedService', function ($q, $http, HOMEPAGE_SERVICE) {

	this.getnewAllCategoriesFilterOfProduct = function (category, subCategoryName, brandName, pricerange, fromVal, toVal, val) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/newproductlist',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "category": category, "from": fromVal, "to": toVal, "brand": brandName, "subcategory": subCategoryName, "pricerange": pricerange, "val": val }

		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

});