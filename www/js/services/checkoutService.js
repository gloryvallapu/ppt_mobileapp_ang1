
pptApp.service('checkoutService', function ($q, $http, HOMEPAGE_SERVICE) {

	this.getDealersListMethod = function (lat_long, pincode) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/shoplist',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "lat_long": lat_long, "pincode": pincode }
		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;

	}
	this.pincodecheckMethod = function (pincode) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/pincdechk?pincode=' + pincode,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }
		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;

	}

	this.saveOrderMethod = function (orderArray) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/checkout',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: orderArray,
		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;

	}

	this.getpayuDetailsMethod = function () {
		var deferred = $q.defer();

		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/payudata',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'payu_secret_key': '4r5s@W' }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};

	this.sendEnquiry = function (shop) {
		var deferred = $q.defer();

		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/enquiry_sent?shop='+shop,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};


});