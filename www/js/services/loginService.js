pptApp.service('loginService', function ($q, $http, HOMEPAGE_SERVICE) {

	this.userAuthentication = function (username, password, ipAddress) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/login',
			headers: { 'Content-Type': 'application/json', 'Authorization': btoa(username + ':' + password), 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8' },
			data: { "ip_address": ipAddress, "user_type": "web" }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.loginpptAuthentication = function (username, password) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/powertex_login',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "username": username, "password": password }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};




});

pptApp.service('registrationService', function ($q, $http, HOMEPAGE_SERVICE) {
	this.userRegistration = function (registrationData, usertype) {
		var deferred = $q.defer();

		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/registration',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8' },
			data: {
				"firstname": registrationData.firstname, "lastname": registrationData.lastname,
				"mobile": registrationData.mobile, "email": registrationData.email, "password": registrationData.password,
				"confirm_password": registrationData.cnfrmpassword, "newsletter": "unchecked",
				"gstnumber": registrationData.gstnumber, "user_type": "web"
			}

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};

	this.verifyOTP = function (otp, mobile, ip) {
		var deferred = $q.defer();

		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/otpverify',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8' },
			data: { "mobile": mobile, "otp": otp, "user_type": "web", "ip_address": ip }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};

	this.resendOTP = function (userId) {
		var deferred = $q.defer();

		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/resendotp?mobile=' + userId,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8' },

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};

});
pptApp.service('logoutService', function ($q, $http, HOMEPAGE_SERVICE) {
	this.userLogout = function (token) {
		var deferred = $q.defer();

		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/logout',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "token": token }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};
});

pptApp.service('userProfileService', function ($q, $http, HOMEPAGE_SERVICE) {
	this.userprofileDetails = function (userid, usertype) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/users_profile?userid=' + userid + '&usertype=' + usertype,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;

	}

	this.editmobilenum = function (userid, mobile_num) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/mobile_update',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "usertype": "web", "mobile": mobile_num, "userid": userid }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;

	}

	this.enterotp = function (userid, mobile, otp, usertype) {
		var deferred = $q.defer();
		$http({
			method: 'PUT',
			url: HOMEPAGE_SERVICE + '/mobile_update',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "userid": userid, "mobile": mobile, "otp": otp, "usertype": usertype }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;

	}
});

pptApp.service('forgotPasswordService', function ($q, $http, HOMEPAGE_SERVICE) {
	this.forgotPassword = function (email) {
		var deferred = $q.defer();

		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/forgortpassword',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "username": email }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};
});