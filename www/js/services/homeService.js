pptApp.service('newHomePageService', function ($q, $http, HOMEPAGE_SERVICE) {

	/* Banners Service start here */
	this.newHomePageMethod = function (link, from) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/newhomepage?from=' + from + '&link=' + link,
			headers: { 'Content-Type': 'application/json', "secret_key": "4r5t@W", 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	/* Offers Service start here */
	this.offersMethod = function (subCatObj, userid) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/offersproducts?upload_subcategory=' + subCatObj + '&userid=' + '',
			headers: { 'Content-Type': 'application/json', "secret_key": "4r5t@W", 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	/* New arraivals Service start here */
	this.newArrivalMethod = function (subCatObj, userid) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/newarrivalsproducts?upload_subcategory=' + subCatObj + '&userid=' + '',
			headers: { 'Content-Type': 'application/json', "secret_key": "4r5t@W", 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8' }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	this.getAllItemscount = function (userID, usertype) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/wishadd_count?userid=' + userID + '&usertype=' + usertype,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};

});
pptApp.service('searchService', function ($q, $http, HOMEPAGE_SERVICE) {

	this.searchProductsMethod = function (productName) {
		var deferred = $q.defer();

		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/newmatchproduct?product_name=' + productName,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};

});