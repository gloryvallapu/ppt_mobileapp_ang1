pptApp.service('product_detailed_service', function ($q, $http, HOMEPAGE_SERVICE) {

	//Product detail page service
    this.getnewDetailsOfProduct = function (productName) {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: HOMEPAGE_SERVICE + '/productdetails?product_name=' + productName,
            headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', "secret_key": "4r5t@W" }

        }).then(function success(data) {
            deferred.resolve(data);
        }, function error(data) {
            deferred.reject(data);
        });
        return deferred.promise;
	};
	

});


pptApp.service('CartService', function ($q, $http, HOMEPAGE_SERVICE) {

	//Add to Cart service
    this.addToCartcustomer = function (cartitems, randomno, userid) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/addtocart_site',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "user_id": userid, "random_no": randomno, "orderitem": cartitems }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

	// this.addToCartmethod = function (prodname, qty, userId, random_no) {
	// 	var deferred = $q.defer();
	// 	$http({
	// 		method: 'POST',
	// 		url: HOMEPAGE_SERVICE + '/initiateorder',
	// 		headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
	// 		data: { "productdescription": prodname, "qty": qty, "user_id": userId, "random_no": "" }
	// 	}).then(function success(data) {
	// 		deferred.resolve(data);

	// 	}, function error(data) {
	// 		deferred.reject(data);
	// 	});
	// 	return deferred.promise;
	// };

	//View Cart service
	this.viewCartcustomer = function (randomno, userid) {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/addtocart_site?userid=' + userid + '&random_no=' + randomno,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};
	

	this.updateviewCartcustomer = function (prodname, qty, random_no, userid) {
		var deferred = $q.defer();
		$http({
			method: 'PUT',
			url: HOMEPAGE_SERVICE + '/addtocart_site',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "user_id": userid, "random_no": random_no, "product_name": prodname, "qty": qty }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};


	this.deleteCartItemcust = function (productname, userid, randomno) {
		var deferred = $q.defer();
		$http({
			method: 'DELETE',
			url: HOMEPAGE_SERVICE + '/addtocart_site',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "product": productname,"userid":userid,"random_no":randomno }
		}).then(function success(data) {
			deferred.resolve(data);
		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};
    

});

pptApp.service('catListService', function ($q, $http, HOMEPAGE_SERVICE) {

	this.getNewCategoriesMethod = function () {
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/newcategories',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }
		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	};

});

pptApp.service('wishListService', function ($q, $http, HOMEPAGE_SERVICE) {

	this.addNewWishListMethod = function (userId, productName) {
		var deferred = $q.defer();

		$http({
			method: 'POST',
			url: HOMEPAGE_SERVICE + '/newwishlist',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "user_id": userId, "product_name": productName }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};

	this.yourWishlistMethod = function (userid) {
		var deferred = $q.defer();

		$http({
			method: 'GET',
			url: HOMEPAGE_SERVICE + '/newyourwishlist?user_id=' + userid,
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};
	
	this.removeWishListItemMethod = function (userId, productName) {
		var deferred = $q.defer();

		$http({
			method: 'DELETE',
			url: HOMEPAGE_SERVICE + '/removewishlist',
			headers: { 'Content-Type': 'application/json', 'Content-type': 'application/x-www-form-urlencoded;charset=utf-8', 'secret_key': '4r5t@W' },
			data: { "user_id": userId, "product_name": productName }

		}).then(function success(data) {
			deferred.resolve(data);

		}, function error(data) {
			deferred.reject(data);

		});

		return deferred.promise;
	};

});