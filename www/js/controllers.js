pptApp.controller('AppCtrl', function ($scope, $ionicModal, newHomePageService, $state, $cordovaInAppBrowser, $rootScope) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login m

  if (window.localStorage.length > 0) {
    $scope.mobile = window.localStorage['mobile'];
    $scope.username = window.localStorage['firstname'];
    $rootScope.token = window.localStorage['token'];
  }

  $scope.homePage = function () {
    $state.go("app.homePage");
  }

  $scope.categories_page = function () {
    $state.go("app.cat_list");
  }
  $scope.loginData = {};


  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function (modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function () {
    $scope.modal.hide();
  };


  // Open the login modal
  $scope.login = function () {
    $state.go("app.login");
  };

  $scope.signUp = function () {
    $state.go("app.registration");
  }

  $scope.emailSent = function () {
    $state.go("app.emailSent");
  }

  $scope.forgetPassword = function () {
    $state.go("app.forgetPassword");
  }

  $scope.changePassword = function () {
    $state.go("app.changePassword");
  }

  $scope.homePage = function () {
    $state.go("app.homePage");
  }

  $scope.header_searchBar = function () {
    $state.go("app.header_searchBar");
  }

  $scope.otp_popup = function () {
    // $state.go("app.otp_popup");
    var browser = $cordovaInAppBrowser.open('http://toolsomg.com/returnpolicy.html#!/');
  }

  $scope.wishlist_page = function () {
    $state.go("app.wishlist_page");
  }

  $scope.upsell_products_page = function () {
    $state.go("app.upsell_products_page");
  }

  $scope.tools_shops_model = function () {
    $state.go("app.tools_shops_model");
  }

  $scope.related_products_page = function () {
    $state.go("app.related_products_page");
  };

  $scope.myprofile = function () {
    $state.go("app.profile");

  }
  $scope.allItemcount = function () {
    newHomePageService.getAllItemscount(window.localStorage['user_id'], window.localStorage['usertype']).then(function (data) {
      if (data.data.status == "success") {
        $rootScope.totalItems = data.data.cart_count;
        $rootScope.wishlistItems = data.data.wishlist_count;
        // alert($rootScope.totalItems);
      }
    })
  };
  if (window.localStorage.length > 0) {
    $scope.allItemcount();

  }
})

  .controller('PlaylistsCtrl', function ($scope) {
    $scope.playlists = [
      { title: 'Reggae', id: 1 },
      { title: 'Chill', id: 2 },
      { title: 'Dubstep', id: 3 },
      { title: 'Indie', id: 4 },
      { title: 'Rap', id: 5 },
      { title: 'Cowbell', id: 6 }
    ];
  })

  .controller('PlaylistCtrl', function ($scope, $stateParams) {
  });
