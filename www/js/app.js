var pptApp = angular.module('pptApp', ['ionic', 'ngCordova'])

  .run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {

      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }
    });
  })

  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider

      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
      })

      .state('app.registration', {
        url: '/registration',
        views: {
          'menuContent': {
            templateUrl: 'templates/registration.html',
          }
        }
      })

      .state('app.emailSent', {
        url: '/emailSent',
        views: {
          'menuContent': {
            templateUrl: 'templates/emailSent.html',
          }
        }
      })

      .state('app.forgetPassword', {
        url: '/forgetPassword',
        views: {
          'menuContent': {
            templateUrl: 'templates/forgetPassword.html',
          }
        }
      })

      .state('app.changePassword', {
        url: '/changePassword',
        views: {
          'menuContent': {
            templateUrl: 'templates/changePassword.html',
          }
        }
      })

      .state('app.homePage', {
        url: '/homePage',
        views: {
          'menuContent': {
            templateUrl: 'templates/homePage.html',
          }
        }
      })

      .state('app.header_searchBar', {
        url: '/header_searchBar',
        views: {
          'menuContent': {
            templateUrl: 'templates/header_searchBar.html',
          }
        }
      })

      .state('app.otp_popup', {
        url: '/otp_popup',
        views: {
          'menuContent': {
            templateUrl: 'templates/otp_popup.html',
          }
        }
      })

      .state('app.wishlist_page', {
        url: '/wishlist_page',
        views: {
          'menuContent': {
            templateUrl: 'templates/wishlist_page.html',
          }
        }
      })

      .state('app.upsell_products_page', {
        url: '/upsell_products_page',
        views: {
          'menuContent': {
            templateUrl: 'templates/upsell_products_page.html',
          }
        }
      })

      .state('app.tools_shops_model', {
        url: '/tools_shops_model',
        views: {
          'menuContent': {
            templateUrl: 'templates/tools_shops_model.html',
          }
        }
      })

      .state('app.related_products_page', {
        url: '/related_products_page',
        views: {
          'menuContent': {
            templateUrl: 'templates/related_products_page.html',
          }
        }
      })

      .state('app.product_Detaile_page', {
        url: '/product_Detaile_page',
        views: {
          'menuContent': {
            templateUrl: 'templates/product_Detaile_page.html',
            controller: 'productDetailController'
          }
        }
      })
      .state('app.product_detail2', {
        url: '/product_detail2',
        views: {
          'menuContent': {
            templateUrl: 'templates/product_detail2.html',
           // controller: 'productDetailController'
          }
        }
      })

      .state('app.categories_page', {
        url: '/categories_page',
        views: {
          'menuContent': {
            templateUrl: 'templates/categories_page.html',
          }
        }
      })

      .state('app.cat_list', {
        url: '/cat_list',
        views: {
          'menuContent': {
            templateUrl: 'templates/cat_list.html',
          }
        }
      })

      .state('app.cart_Page', {
        url: '/cart_Page',
        views: {
          'menuContent': {
            templateUrl: 'templates/cart_Page.html',
          }
        }
      })

      .state('app.specifications_page', {
        url: '/specifications_page',
        views: {
          'menuContent': {
            templateUrl: 'templates/specifications_page.html',
          }
        }
      })

      .state('app.shipping&billing_page', {
        url: '/shipping&billing_page',
        views: {
          'menuContent': {
            templateUrl: 'templates/shipping&billing_page.html',
          }
        }
      })

      .state('app.login', {
        url: '/login',
        views: {
          'menuContent': {
            templateUrl: 'templates/login.html',
          }
        }
      })

      .state('app.editShippingModal', {
        url: '/editShippingModal',
        views: {
          'menuContent': {
            templateUrl: 'templates/editShippingModal.html',
          }
        }
      })

      .state('app.payu', {
        url: '/payu',
        views: {
          'menuContent': {
            templateUrl: 'templates/payu.html',
          }
        }
      })

      .state('app.all_offers', {
        url: '/all_offers',
        views: {
          'menuContent': {
            templateUrl: 'templates/all_offers.html',
          }
        }
      })

      .state('app.all_newarraivals', {
        url: '/all_newarraivals',
        views: {
          'menuContent': {
            templateUrl: 'templates/all_newarraivals.html',
          }
        }
      })

      .state('app.profile', {
        url: '/profile',
        views: {
          'menuContent': {
            templateUrl: 'templates/profile.html',
          }
        }
      })





    /* -------------------- */

    // .state('app.browse', {
    //   url: '/browse',
    //   views: {
    //     'menuContent': {
    //       templateUrl: 'templates/browse.html'
    //     }
    //   }
    // })
    // .state('app.playlists', {
    //   url: '/playlists',
    //   views: {
    //     'menuContent': {
    //       templateUrl: 'templates/playlists.html',

    //     }
    //   }
    // })

    // .state('app.single', {
    //   url: '/playlists/:playlistId',
    //   views: {
    //     'menuContent': {
    //       templateUrl: 'templates/playlist.html',

    //     }
    //   }
    // });

    $urlRouterProvider.otherwise('/app/homePage');
  });
